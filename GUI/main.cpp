#include <iostream>
#include <stdio.h>
#include <gtk/gtk.h>
#include "SerialClass.h"


GtkWidget *window, *container, *entry, *label, *button; /** Janela, Container, Campo de entrada, Campo de exibição, Botão **/



char deuBom[7] ;




void iniciaComunicacaoSerial(){
    Serial *pGalileo = new Serial("COM12"); // insira a porta USB correta (COMX) .


    char senha[50]; //Cria buffer de saida
    pGalileo->WriteData(senha,50);


    char leitura[50]; // Cria buffer de entrada
    pGalileo-> ReadData(leitura,50); // leitura indicando Buffer de entrada e tamanho, realiza a leitura;.


}


















// função para inicialização dos elementos da GUI


// função de callback para evento de fechamento da janela
static void destroy_callback( GtkWidget *widget, gpointer   data ){
    gtk_main_quit ();
}
// função de callback para evento de clique no botão
static void button_callback( GtkWidget *widget, gpointer   data )
{
    const char *text;
    text = gtk_entry_get_text( GTK_ENTRY (entry));
    char text_out[1024] = "Foi informado ";
    strcat( text_out, text);
    gtk_label_set_text( GTK_LABEL (label), text_out);
}

void init_gui()
{
    // criação da janela principal
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_size_request (GTK_WIDGET (window), 200, 100);
    gtk_window_set_title (GTK_WINDOW (window), "GTK Hello World");
    // Associa callback para o evento de fechamento da janela
    g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK(destroy_callback), NULL);
    // criação do container, ao qual os demais widgets sao subordinados
    container = gtk_fixed_new ();
    gtk_container_add (GTK_CONTAINER (window), container);
    gtk_widget_show (container);

    // criação do campo de edição
    entry = gtk_entry_new ();
    gtk_fixed_put (GTK_FIXED (container), entry, 5, 5);
    gtk_widget_show (entry);

    // criação do botão
    button = gtk_button_new_with_label ("Processar");
    // Associa callback para o evento de pressionamento
    g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK(button_callback), NULL);
    gtk_fixed_put (GTK_FIXED (container), button, 5, 35);
    gtk_widget_show (button);

    // criação da etiqueta de exibição
    label = gtk_label_new ("");
    gtk_fixed_put (GTK_FIXED (container), label, 5, 75);
    gtk_widget_show (label);

    // exibe janela principal
    gtk_widget_show (window);
}


int main( int argc, char *argv[])
{
//    GtkWidget *window;  // pointer para widget tipo janela
//    gtk_init(&argc, &argv);  // inicializa GTK
//
//    window = gtk_window_new(GTK_WINDOW_TOPLEVEL); // cria janela
//    gtk_widget_show(window);  // mostra janela
//    gtk_main();  // vai para loop de espera de eventos
    std::cout << "Hello, World!" << std::endl;
    Serial *h = new Serial("COM5");

    char envia;
    scanf(&envia,"%c");
    h->WriteData(&envia,1);
    return 0;

}